<?php
namespace YoutubeVideo;
use PHPTool\Rurl\Rurl;
use PHPTool\TextMatch\Matcher;
use PHPTool\TextMatch\MatchPattern;

/**
 * 获取下载地址
 */
class DownloadAddress
{
    public  $rurl;

    public $proxy = '';

    /**
     * 优先级（根据质量）
     * [min,max]  min:最低分辨率 max:最高分辨率
     */
    public $quality = ['480,720'];

    public function __construct()
    {
        $this->rurl = new Rurl();
        $this->rurl->setRequestHeaderArray([
            'Accept' => '*/*',
            'Origin' => 'https://www.y2mate.com',
            'X-Requested-With' => 'XMLHttpRequest',
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
        ]);
    }

    /**
     * 设置代理
     */
    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
        $this->rurl->setOption(CURLOPT_PROXY, $this->proxy);
    }

    /**
     * 根据播放地址获取视频的下载地址
     * 参数是youtube的播放地址 
     */
    public function get($youtubeplayurl)
    {
        //1.分析地址，得到下载列表
        $list = $this->parse($youtubeplayurl);

        //2.选择一个下载地址配置(根据质量 选择)
        $selected_down = $this->getDownURLParams($list);

        //3.获取下载地址 
        $param = [
            'ftype' => $selected_down['ftype'],
            'fquality' => $selected_down['fquality'],
            '_id' => $selected_down['_id'],
            'type' => $selected_down['type'],
            'v_id'  => $selected_down['v_id'],
            'ajax' => $selected_down['ajax'],
            'token' => $selected_down['token'] 
        ];
        $downloadurl = $this->getDownloadURL($param);
        return [
            'download_url' => $downloadurl,
            'ftype' => $selected_down['ftype'],
            'fsize' => $selected_down['fsize']
        ];
    }

    /**
     * 获取下载地址
     */
    public function getDownloadURL(&$param)
    {
        $post_query = http_build_query($param);
        $convert_url = 'https://www.y2mate.com/mates/convert';
        $result = $this->rurl->post($convert_url, $param);
        $this->rurl->close();
        $data = json_decode($result->contents, true);
        $pattern = new MatchPattern();
        $pattern->setFields([
            'download_url'
        ]);
        $pattern->setPatterns([
            '<a href="{download_url?}" rel="nofollow" type="button" class="btn btn-success btn-file">'
        ]);
        $matcher = new Matcher($pattern);
        $res = $matcher->match($data['result']);
        $data = $res->getDataOneValue();
        return $data['download_url'];
    }

    /**
     * 根据优先级 获取一组下载参数
     */
    public function getDownURLParams($list)
    {
        $usedparam = $list[0];
        foreach($this->quality as $clarity)
        {
            list($min, $max) = explode(',', $clarity);
            foreach($list as $param)
            {
                if($param['fquality']>=$min && $param['fquality']<=$max)
                {
                    $usedparam = $param;
                    break 2;
                }
            }
        }
        return $usedparam;
    }

    /**
     * 分析地址,获取视频下载列表
     */
    public function parse($youtubeplayurl)
    {
        $url_info = parse_url($youtubeplayurl);
        $youtube_url_query = $this->queryToArray($url_info['query']);

        // $parse_address = 'https://www.y2mate.com/mates/en100/analyze/ajax';
        // $parse_address = 'https://www.y2mate.com/mates/en450/analyze/ajax';
        $parse_address = 'https://www.y2mate.com/mates/analyze/ajax';
        $params = [
            'url' => $youtubeplayurl,
            'q_auto' => 0,
            'ajax' => 1
        ];

        $referer = 'https://www.y2mate.com/youtube/'.$youtube_url_query['v'];
        $this->rurl->setRequestHeader('referer', $referer);

        $res = $this->rurl->post($parse_address, http_build_query($params));

        $data = json_decode($res->contents, true);
        // file_put_contents('./content.html', $data);

        return $this->getDownList($data['result']);
    }

    /**
     * 提取下载列表
     */
    public function getDownList($contents)
    {
        $pattern = new MatchPattern();
        $pattern->setFields([
            'fsize',
            'ftype',
            'fquality',
            '_id',
            'v_id',
        ]);
        $pattern->setPatterns([
            '<tr> <td>{*?}</td> <td>{fsize?}</td> <td class="txt-center"> <a href="javascript:void(0)" rel="nofollow" type="button" class="btn btn-success" data-toggle="modal" data-target="#progress" data-ftype="{ftype?}" data-fquality="{fquality?}"> <i class="glyphicon glyphicon-download-alt"></i>&nbsp; Download </a> </td> </tr>',
            'var k__id = "{_id?}";',
            'var k_data_vid = "{v_id?}";',
        ]);
        $matcher = new Matcher($pattern);
        $result = $matcher->match($contents);
        $data = $result->getData();
        $params = [];
        foreach($data['fquality'] as $i=>$fquality)
        {
            array_push($params, [
                'fsize' => $data['fsize'][$i],
                'ftype' => $data['ftype'][$i],
                'fquality' => $data['fquality'][$i],
                '_id' => $data['_id'][0],
                'type' => 'youtube',
                'v_id'  => $data['v_id'][0],
                'ajax' => 1,
                'token' => ''
            ]);
        }
        return $params;
    }

    /**
     * 将url的query部分 转成数组
     */
    public function queryToArray($query)
    {
        $query_arr = explode('&', $query);
        foreach($query_arr as $item)
        {
            $arr = [];
            $k_v = explode('=', $item)?:[];
            if($k_v[0]??'')
            {
                $arr[$k_v[0]] =  $k_v[1]??'';
            }
            return $arr;
        }
    }
}

