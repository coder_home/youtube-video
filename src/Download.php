<?php
namespace YoutubeVideo;

use PHPTool\Rurl\Rurl;
use YoutubeVideo\DownloadAddress;
/**
 * 下载视频
 */
class Download
{
    public $proxy = '';

    /**
     * 下载并保存
     * $url 视频的播放地址
     * $title 视频标题用于文件名
     */
    public function load($url, $title, $savedir='')
    {
        $downurl_info = $this->getDownloadURL($url);
        // print_r($downurl_info);
        // exit;
        $savefile = rtrim($savedir, '\/') . '/' . $title . '.' . $downurl_info['ftype'];
        /* if(file_exists($savefile))
         * {
         * } */
        $this->createFile($savefile);

        $fp = fopen($savefile, 'w');
        if(false === $fp)
        {
            throw new \Exception('无法打开 或 创建文件 ' . $savefile);
        }

        $rurl  = new Rurl();
        $rurl->setRequestHeader('referer', 'https://www.y2mate.com/');
        $rurl->setRequestHeader('user-agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36');
        $rurl->setOption(CURLOPT_TIMEOUT, 0);
        $rurl->setOption(CURLOPT_CONNECTTIMEOUT, 3);
        $rurl->setOption(CURLOPT_FILE, $fp);
        $rurl->setOption(CURLOPT_RETURNTRANSFER, false);
        $rurl->setOption(CURLOPT_PROXY, $this->proxy);
        $downurl = $downurl_info['download_url'];
        /* echo $downurl; */
        do
        {
            $result = $rurl->get($downurl);
            if($location = $result->getHeader('location'))
            {
                $downurl = $location;
            }
        }while($location);
        fclose($fp);
        $rurl->close();
        $result->saveFile = $savefile;
        $result->downurlInfo = $downurl_info;
        return $result;
        // file_put_contents($savefile, $result->contents);
    }

    /**
     * 创建文件
     */
    public function createFile($filename)
    {
        $dirname = dirname($filename);
        if(!file_exists($dirname))
        {
            $succ = mkdir($dirname, 0777, true);
            if(!$succ)
            {
                throw new \Exception('尝试创建目录' . $dirname . '失败');
            }
            $succ = touch($filename);
            if(!$succ)
            {
                throw new \Exception('尝试创建目录' . $dirname . '失败');
            }
        }
    }

    /**
     * 获取下载地址 
     */
    public function getDownloadURL($playerurl)
    {
        $dla = new DownloadAddress();
        $dla->setProxy($this->proxy);
        return $dla->get($playerurl);
    }

    /**
     * 设置代理
     */
    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }
}
