<?php
namespace YoutubeVideo;

use PHPTool\TextMatch\Matcher;
use PHPTool\TextMatch\MatchPattern;
use PHPTool\Rurl\Rurl;

/**
 * 根据列表页地址获取 播放地址列表信息
 */
class MatchPlayingUrl
{
    public $proxy = '';

    public $nodeCommand = 'node';

    /**
     * 设置node命令的路径
     */
    public function nodeCommand($string)
    {
        $this->nodeCommand = $string;
    }

    public function get($url)
    {
        // 获取列表页面
        $contents = $this->getPage($url);
        // file_put_contents(__DIR__ . '/../download/list.html', $contents);
        // $contents = file_get_contents(__DIR__ . '/../download/list.html');

        //匹配信息
        $data = $this->findData($contents);
        if(empty($data['total']))
        {
            return [];
        }

        $items = [];

        //获取当前列
        $list = $this->findList($contents);
        $items = array_merge($items, $list['list']);

        $cparam = $data['next_info'];
        while(!empty($list['next_info']))
        {
            // 生成下一页的url 和 post
            $param = $list['next_info'];
            $next = $this->createNext($cparam, $param);

            //获取下一页数据
            $next_data = $this->getNextData($next['next_url'], $next['post_data']);
            $next_data = json_decode($next_data, true);

            //取出需要的数据
            $list = $this->getUsefulNextData($next_data);
            $items = array_merge($items, $list['list']);
        }
        return $items;
    }

    /**
     * 获取列表页面
     */
    public function getPage($url)
    {
        $request = $this->request();
        $result = $request->get($url);
        $request->close();
        if($result->errorNum == 0)
        {
            return $result->contents;
        }
        else
        {
            throw new \Exception("请求页面时出错({$result->uri})。错误码:{$result->errorNum}; 错误信息:{$result->errorMessage}");
        }
    }

    /**
     * 取出页面中需要的数据
     */
    public function findData($contents)
    {
        $pattern = new MatchPattern();
        $pattern->setLeftLimit('{-');
        $pattern->setRightLimit('-}');
        $fields = [
            'total',
            'code'
        ];

        $pattern->setFields($fields);
        $patterns = [
            '"stats":[{"runs":[{"text":"{-total?-}"}',
            '(function() {window.ytplayer={};{- -}ytcfg.set({-code-}); window.ytcfg.obfuscatedData_ = [];var setMessage=function(msg){if(window.yt&&yt.setMsg)yt.setMsg(msg);else{window.ytcfg=window.ytcfg||{};ytcfg.msgs=msg}};'
        ];
        $pattern->setPatterns($patterns);

        $matcher = new Matcher($pattern);
        $result = $matcher->match($contents)->getDataOneValue();
        $code = 'var a = '.$result['code'].';var b = JSON.stringify(a);console.log(b)';
        $data = $this->execNode($code);
        $data = json_decode($data, true);

        $next_info['context']['client'] = $data['INNERTUBE_CONTEXT']['client'];
        $next_info['context']['user'] = $data['INNERTUBE_CONTEXT']['user'];
        $next_info['context']['request'] = $data['INNERTUBE_CONTEXT']['request'];
        $next_info['apiKey'] = $data['INNERTUBE_API_KEY'];
        $total = $result['total'];
        return compact('total',  'next_info');
    }

    /**
     * 取出列表数据
     */
    public function findList($contents)
    {
        $pattern = new MatchPattern();
        $pattern->setLeftLimit('{-');
        $pattern->setRightLimit('-}');
        $fields = [
            'list_code',
        ];

        $pattern->setFields($fields);
        $patterns = [
            '>var ytInitialData = {-list_code?-}</script>'
        ];
        $pattern->setPatterns($patterns);

        $matcher = new Matcher($pattern);
        $result = $matcher->match($contents)->getDataOneValue();
        $code  = $result['list_code'];
        $code = 'var a='.$code.';';
        $code .= 'var list = JSON.stringify(a.contents.twoColumnBrowseResultsRenderer.tabs[0].tabRenderer.content.sectionListRenderer.contents[0].itemSectionRenderer.contents[0].playlistVideoListRenderer);console.log(list)';
        $data = $this->execNode($code);
        $data = json_decode($data, true);
        $result = [
            'list'  => [],
            'next_info' => null,
        ];
        foreach($data['contents'] as $item)
        {
            if(isset($item['playlistVideoRenderer']))
            {
                $playinfo = $item['playlistVideoRenderer'];
                $result['list'][] = [
                    'lengthSeconds' => $playinfo['lengthSeconds'],
                    'playurl' => $playinfo['navigationEndpoint']['commandMetadata']['webCommandMetadata']['url'],
                    'title' => $playinfo['title']['runs'][0]['text'],
                ];
            }
            else if(isset($item['continuationItemRenderer']))
            {
                $result['next_info']['continuation'] = $item['continuationItemRenderer']['continuationEndpoint']['continuationCommand']['token'];
                $result['next_info']['clickTrackingParams'] = $data['trackingParams'];
                $result['next_info']['playlistId'] = $data['playlistId'];
                $result['next_info']['apiUrl'] = $item['continuationItemRenderer']['continuationEndpoint']['commandMetadata']['webCommandMetadata']['apiUrl'];
                $result['next_info']['sendPost'] = $item['continuationItemRenderer']['continuationEndpoint']['commandMetadata']['webCommandMetadata']['sendPost'];
            }
        }
        return $result;
    }

    /**
     * 获取下一页数据
     */
    public  function getNextData($next_url, $post_data)
    {
        $request = $this->request();
        $request->setRequestHeader('content-type', 'application/json; charset=UTF-8');
        $result = $request->post($next_url, json_encode($post_data));
        $request->close();

        if($result->errorNum == 0)
        {
            return $result->contents;
        }
        else
        {
            throw new \Exception("请求时出错({$result->uri})。错误码:{$result->errorNum}; 错误信息:{$result->errorMessage}");
        }
    }

    /**
     * 生成下一页的url 和 postData
     * @param array $cparam 公共参数
     * @param array $param
     */
    public function createNext($cparam, $param)
    {
        //获取下一些相关数据
        $next_url = 'https://www.youtube.com' . $param['apiUrl'] . '?key=' . $cparam['apiKey'];
        $post_data['context']['client'] = $cparam['context']['client'];
        $client_other = [
            "screenWidthPoints" => 1920,
            "screenHeightPoints" => 306,
            "screenPixelDensity" => 1,
            "screenDensityFloat" => 1,
            "utcOffsetMinutes" => 480,
            "userInterfaceTheme" => "USER_INTERFACE_THEME_LIGHT",
            "connectionType" => "CONN_CELLULAR_4G",
            "memoryTotalKbytes" => "8000000",
            "mainAppWebInfo" => [
                "graftUrl" => "https://www.youtube.com/playlist?list=" . $param['playlistId'],
                "webDisplayMode" => "WEB_DISPLAY_MODE_BROWSER",
                "isWebNativeShareAvailable" => true
            ]
        ];
        $post_data['context']['client'] = array_merge($post_data['context']['client'], $client_other);
        $post_data['context']['user'] = $cparam['context']['user'];
        $post_data['context']['request'] = $cparam['context']['request'];
        $post_data['context']['request']['internalExperimentFlags'] = [];
        $post_data['context']['request']['consistencyTokenJars'] = [];
        $post_data['context']['clickTracking']['clickTrackingParams'] = $param['clickTrackingParams'];
        $post_data['context']['adSignalsInfo'] = [
            "params" => [
                [
                    "key" => "dt",
                    "value" => substr(str_replace('.', '', microtime(true)), 0, 13)
                ],
                [
                    "key" => "flash",
                    "value" => "0"
                ],
                [
                    "key" => "frm",
                    "value" => "0"
                ],
                [
                    "key" => "u_tz",
                    "value" => "480"
                ],
                [
                    "key" => "u_his",
                    "value" => "11"
                ],
                [
                    "key" => "u_h",
                    "value" => "1080"
                ],
                [
                    "key" => "u_w",
                    "value" => "1920"
                ],
                [
                    "key" => "u_ah",
                    "value" => "1040"
                ],
                [
                    "key" => "u_aw",
                    "value" => "1920"
                ],
                [
                    "key" => "u_cd",
                    "value" => "24"
                ],
                [
                    "key" => "bc",
                    "value" => "31"
                ],
                [
                    "key" => "bih",
                    "value" => "306"
                ],
                [
                    "key" => "biw",
                    "value" => "1904"
                ],
                [
                    "key" => "brdim",
                    "value" => "0,0,0,0,1920,0,1920,1040,1920,306"
                ],
                [
                    "key" => "vis",
                    "value" => "1"
                ],
                [
                    "key" => "wgl",
                    "value" => "true"
                ],
                [
                    "key" => "ca_type",
                    "value" => "image"
                ]
            ]
        ];
        $post_data['continuation'] = $param['continuation'];
        return compact('next_url', 'post_data');
    }

    /**
     * 取出nextData中有用的数据
     */
    public function getUsefulNextData($data)
    {
        $result = [
            'list'  => [],
            'next_info' => null,
        ];
        foreach($data['onResponseReceivedActions'][0]['appendContinuationItemsAction']['continuationItems'] as $item)
        {
            if(isset($item['playlistVideoRenderer']))
            {
                $playinfo = $item['playlistVideoRenderer'];
                $result['list'][] = [
                    'lengthSeconds' => $playinfo['lengthSeconds'],
                    'playurl' => $playinfo['navigationEndpoint']['commandMetadata']['webCommandMetadata']['url'],
                    'title' => $playinfo['title']['runs'][0]['text'],
                ];
            }
            else if(isset($item['continuationItemRenderer']))
            {
                $result['next_info']['continuation'] = $item['continuationItemRenderer']['continuationEndpoint']['continuationCommand']['token'];
                $result['next_info']['clickTrackingParams'] = $data['onResponseReceivedActions'][0]['clickTrackingParams'];
                $result['next_info']['playlistId'] = $data['onResponseReceivedActions'][0]['appendContinuationItemsAction']['targetId'];
                $result['next_info']['apiUrl'] = $item['continuationItemRenderer']['continuationEndpoint']['commandMetadata']['webCommandMetadata']['apiUrl'];
                $result['next_info']['sendPost'] = $item['continuationItemRenderer']['continuationEndpoint']['commandMetadata']['webCommandMetadata']['sendPost'];
            }
        }
        return $result;
    }


    /**
     * exec node
     */
    public function execNode($code)
    {
        $tmpdir = sys_get_temp_dir();
        // $tmpdir = './';
        $tmpfile = tempnam($tmpdir, 'youtube_video__');
        file_put_contents($tmpfile,  $code);
        $command = $this->nodeCommand . ' '.$tmpfile;
        exec($command, $output);
        unlink($tmpfile);
        return  $output[0];
    }

    /**
     * rurl对象
     */
    public  function request()
    {
        $rurl = new Rurl();
        if($this->proxy)
        {
            $rurl->setOption(CURLOPT_PROXY, $this->proxy);
        }
        return $rurl;
    }

    /**
     * 设置代理
     */
    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }
}

